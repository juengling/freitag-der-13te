Es find alles mit der Behauptung eines Twitterers an:

    Rein statistisch betrachtet ist die Häufigkeit schwerer Unglücke an Freitag,
    dem dreizehnten tatsächlich höher als beispielsweise an Freitag dem zwölften:
    Er kommt im Kalender häufiger vor.

Dieses Projekt hat den Zweck, dies zu beweisen oder zu widerlegen.