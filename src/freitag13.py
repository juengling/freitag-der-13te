'''
Created on 18.07.2018

@author: Chris
'''
import datetime


def main():

    all_friday12 = 0
    all_friday13 = 0

    for y in range(1, 3001):
        friday12 = 0
        friday13 = 0

        for m in range(1, 13):
            for d in range(1, 32):
                try:
                    # If this is not a valid date ...
                    mydate = datetime.date(y, m, d)
                except ValueError:
                    # ... break the day-loop and continue with next month
                    break

                if is_fr_12(mydate):
                    friday12 += 1
                    all_friday12 += 1

                if is_fr_13(mydate):
                    friday13 += 1
                    all_friday13 += 1

        print('{}: {}, {}'.format(mydate.year, friday12, friday13))

    print('Over all: {}, {} = {} %'.format(all_friday12, all_friday13,
                                           (all_friday13 / all_friday12 - 1) * 100))


def is_fr_12(d):
    '''
    Check if date is friday 13th

    :param d: Date to be checked
    :return: True, if friday 13th, otherwise False
    '''
    return d.day == 12 and d.weekday() == 4


def is_fr_13(d):
    '''
    Check if date is friday 13th

    :param d: Date to be checked
    :return: True, if friday 13th, otherwise False
    '''
    return d.day == 13 and d.weekday() == 4


def is_th_13(d):
    '''
    Check if date is thursday 13th

    :param d: Date to be checked
    :return: True, if thursday 13th, otherwise False
    '''
    return d.day == 13 and d.weekday() == 3


if __name__ == '__main__':
    main()
